import React from 'react'
import queryString from 'query-string';
import authService from '../auth-service';

import {getIn, setIn} from '../utilities/common';
import {Messages} from '../utilities/components';



export default class UserRecoverPassword extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      password1: undefined,
      password2: undefined,
      dialogPurpose: 0,
      errorMessage: undefined,
      infoMessage: undefined,
      tokenUsable: false,
    }
  }

  getToken() {
    const { search } = this.props.location;
    const { token } = queryString.parse(search);
    return token;
  }
  
  withShowError(promise) {
    return promise.catch(error => {
      this.setState({
        errorMessage: getIn(error, ['response', 'data', 'status'], 'Something went wrong :('),
      });
    })
  }  

  runWithShowError(fcn) {
    return this.withShowError(fcn())
  }  

  async componentWillMount() {
    this.runWithShowError(async() => {
      try {
        this.setState({tokenUsable: false});
        await authService.checkToken(this.getToken());
      } catch(e) {
        throw e;
      }
      this.setState({tokenUsable: true});
    })
  }

  dismissError = (event) => {
    this.setState({errorMessage: undefined});
    event.preventDefault();
  }

  dismissInfo = (event) => {
    this.setState({infoMessage: undefined});
    event.preventDefault();
  }

  setPassword = async (event) => {
    this.runWithShowError(async() => {
      event.preventDefault();
      await authService.resetPassword(this.getToken(), this.state.password1);
      this.setState({
        infoMessage: "Your password has ben reset.",
        resetSuccessful: true,
      });
    });
  }

  arePasswordsValid() {
    return (
      this.state.password1 &&
      this.state.password2 &&
      this.state.password1 === this.state.password2
    )
  }

  handleChange = (e) => {
    this.setState({[e.target.name]: e.target.value});
  }

  render() {
    return (
      <div className="content">
        <div className="col-md-6 col-md-offset-3">
          <div className="box box-primary password-recovery overlay-continer">
            
            <div className="box-header with-border">
              <h3 className="box-title">Recover password</h3>
            </div>

            <Messages {...this.state} {...this} />

            {(this.state.tokenUsable && !this.state.resetSuccessful) && <form onSubmit={(e) => this.handleSubmit(e)}>
              <div className="box-body">
                <div className="form-group">
                  <label>New Password</label>
                  <input name="password1" type="password" className="form-control" onChange={this.handleChange} />
                </div>
                <div /*className={validClassname}*/>
                  <label>Confirm New Password <i /*className={checkClassname}*/></i></label>
                  <input name="password2" type="password" className="form-control" onChange={this.handleChange}/>
                </div>
              </div>
              {!this.arePasswordsValid() && <div className="box-footer">
                Passwords must not be empty and have to be the same.
              </div>}
              {this.arePasswordsValid() && <div className="box-footer">
                <button type="submit" className="btn btn-primary pull-right" onClick={this.setPassword}>Submit</button>
              </div>}
            </form>}
            {(!this.state.tokenUsable || this.state.resetSuccessful) && <div className="box-footer">
              <a className="btn btn-primary pull-right" href="#/">Go home</a>
            </div>}
          </div>
        </div>
      </div>
    )
  }
}