import React from "react"
import { Link } from "react-router-dom"
import ROUTES from "../routes"

import contractsService from "./contracts-service"
import { price, dateToLocal } from "../format"
import authService from "../auth-service"

class ContractDetail extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      contract: {},
      customer: [],
      comments: [],
      newCommentText: ""
    }
  }

  componentWillMount() {
    this.load(this.props.match.params.id)
    this.loadComments(this.props.match.params.id)
  }

  componentWillReceiveProps(newProps) {
    if (this.props.match.params.id !== newProps.match.params.id) {
      this.load(newProps.match.params.id)
      this.loadComments(newProps.match.params.id)
    }
  }

  async load(id) {
    let res = await contractsService.getContract(id)

    this.setState({
      contract: res.data,
      customer: res.data.customer
    })
  }

  async loadComments(id) {
    let res = await contractsService.getContractComments(id)

    this.setState({
      comments: res.data
    })
  }

  create() {
    contractsService.createContract(this.state.contract).then(res => {
      this.props.history.push(ROUTES.CONTRACT_LISTING)
    })
  }

  createComment() {
    contractsService
      .createContractComment(this.state.contract.id, {
        id: "",
        text: this.state.newCommentText
      })
      .then(res => {
        this.setState({ comments: this.state.comments.concat(res.data) })
      })
    this.setState({ newCommentText: "" })
  }

  updateNewComment(e) {
    this.setState({ newCommentText: e.target.value })
  }

  render() {
    const contract = this.state.contract
    const customer = this.state.customer
    const reversedComments = Object.assign([], this.state.comments)
    reversedComments.reverse()

    return (
      
      <div className="box box-default">
        <div className="box-header">
          <h3 className="box-title">Contract Details</h3>
        </div>
        <div className="box-body">
          <div className="col-md-6">
            <div className="btn-group margin-bottom">
              <Link className="btn btn-default" to={ROUTES.getUrl(ROUTES.CONTRACT_EDIT, { id: contract.id })}>
                Edit
              </Link>
              <Link className="btn btn-default" to={ROUTES.getUrl(ROUTES.CONTRACT_LISTING)}>
                Delete
              </Link>
            </div>
            <table className="table table-borderless margin-bottom">
              <tbody>
                <tr>
                  <th className="col-md-1 text-right">Name</th>
                  <td className="col-md-11">{contract.name}</td>
                </tr>
                <tr>
                  <th className="text-right">Description</th>
                  <td>{contract.description}</td>
                </tr>
                <tr>
                  <th className="text-right">Price</th>
                  <td>{price(contract.price)}</td>
                </tr>
                <tr>
                  <th className="text-right">Customer</th>
                  <td>
                    <Link to={ROUTES.getUrl(ROUTES.CUSTOMER_DETAIL, {id: contract.customerId})}>
                      {customer.name}
                    </Link>
                  </td>
                </tr>
                <tr>
                  <th className="text-right">Created</th>
                  <td>{dateToLocal(contract.dateCreated)}</td>
                </tr>
                <tr>
                  <th className="text-right">Edited</th>
                  <td>{contract.dateEdited ? dateToLocal(contract.dateEdited) : ""}</td>
                </tr>
                <tr>
                  <th className="text-right">Deadline</th>
                  <td>{contract.deadline ? dateToLocal(contract.deadline) : ""}</td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="col-md-6 margin-bottom">
            <div className="comments">
              <label className="lead">Comments</label>
              <div className="input-group">
                <input className="form-control"
                  placeholder="New comment"
                  value={this.state.newCommentText}
                  onChange={e => this.updateNewComment(e)}
                />
                <span className="input-group-btn">
                  <button
                    type="button"
                    className="btn btn-default"
                    onClick={() => this.createComment()}
                  >
                    Add
                  </button>
                </span>
              </div>
            </div>
            {reversedComments.map(c => (
              <div className="comments" key={c.id}>
                {dateToLocal(c.date)} <b>{authService.user.username}</b>
                <br/>
                {c.text}
              </div>
            ))}
          </div>
        </div>
      </div>
    )
  }
}

export default ContractDetail