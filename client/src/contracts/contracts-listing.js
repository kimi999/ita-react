import React from 'react'
import { Link } from 'react-router-dom'
import ROUTES from '../routes'
import ContractsService from './contracts-service'
import _ from 'lodash'
import { price, dateToLocal } from '../format'
import DatePicker from 'react-datepicker';
import moment from 'moment';
import Select from 'react-select';

import 'react-select/dist/react-select.css';
import 'react-datepicker/dist/react-datepicker.css';

class ContractsListing extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      dataPickerTimes: {
        currentTimeFromDC: moment('2017-01-01'),
        currentTimeToDC: moment(),        // nastaví dnešní datum pro DataPicker pro data created
        currentTimeFromDE: moment('2017-01-01'),
        currentTimeToDE: moment(),        // nastaví dnešní datum pro DataPicker pro data edited
        currentTimeFromDL: moment('2017-01-01'),
        currentTimeToDL: moment(),        // nastaví dnešní datum pro DataPicker pro deadline
      },

      contracts: [],
      filter: {
        q: undefined,                 // full-textové vyhledávání
        status: undefined,            // stav zakázky
        dateCreated: undefined,       // datum vytvoření
        dateEdited: undefined,        // datum editace
        deadline: undefined,          // termín dokončení
        dateCreated_gte: undefined,   // datum vytvoření je VĚTŠÍ nebo ROVNO
        dateEdited_gte: undefined,    // datum editace je VĚTŠÍ nebo ROVNO
        deadline_gte: undefined,      // termín dokončení je VĚTŠÍ nebo ROVEN
        dateCreated_lte: undefined,   // datum vytvoření je MENŠÍ nebo ROVNO
        dateEdited_lte: undefined,    // datum editace je MENŠÍ nebo ROVNO
        deadline_lte: undefined       // termín dokončení je MENŠÍ nebo ROVEN
      }
    }

    this.handleFilter = this.handleFilter.bind(this)

    this.handleStatusFilter = this.handleStatusFilter.bind(this)

    this.loadDebounced = _.debounce(this.load, 200)
  }

  componentWillMount() {
    this.load()
  }

  async load() {
    let res = await ContractsService.filterContracts(this.state.filter)

    this.setState({
      contracts: res.data
    })
  }

  handleSubmit(event) {
    event.preventDefault()
  }

  handleFilter(event) {
    let filter = this.state.filter
    filter[event.target.name] = event.target.value
      ? event.target.value
      : undefined
    this.forceUpdate()
    this.loadDebounced()
  }

  handleStatusFilter(event) {
    if (event == null) {
      let filter = this.state.filter
      filter["status"] = undefined
    } else {
      let filter = this.state.filter
      filter["status"] = event.value ? event.value : undefined
    }
    this.forceUpdate()
    this.loadDebounced()
  }

  handleDateFilterFrom(date, filterBy, currentTime) { //nastaví dataPicker OD
    let dataPickerTimes = this.state.dataPickerTimes
    dataPickerTimes[currentTime] = date

    let filter = this.state.filter
    filter[filterBy + '_gte'] = (date.toISOString() ? date.toISOString() : undefined)
    this.forceUpdate()
    this.loadDebounced()
  }

  handleDateFilterTo(date, filterBy, currentTime) { //filterBy = string s hodnotou data edited, created nebo deadline
    let dataPickerTimes = this.state.dataPickerTimes
    dataPickerTimes[currentTime] = date   //nastaví datum pro dataPicker, currentTime = string s hodnotou pro dataPickerTimes

    let filter = this.state.filter
    filter[filterBy + '_lte'] = (date.toISOString() ? date.toISOString() : undefined) //nastaví filter
    this.forceUpdate()
    this.loadDebounced()
  }

  handleDateFilterReset(filterBy, currentTimeShort) {
    if (filterBy == null) {
      let filter = this.state.filter //pro kompletní reset
      filter['dateCreated_gte'] = undefined
      filter['dateCreated_lte'] = undefined
      filter['dateEdited_gte'] = undefined
      filter['dateEdited_lte'] = undefined
      filter['deadline_gte'] = undefined
      filter['deadline_lte'] = undefined

      let dataPickerTimes = this.state.dataPickerTimes
      dataPickerTimes['currentTimeFromDC'] = moment('2017-01-01')
      dataPickerTimes['currentTimeToDC'] = moment()
      dataPickerTimes['currentTimeFromDE'] = moment('2017-01-01')
      dataPickerTimes['currentTimeToDE'] = moment()
      dataPickerTimes['currentTimeFromDL'] = moment('2017-01-01')
      dataPickerTimes['currentTimeToDL'] = moment()
    } else {
      let filter = this.state.filter //pro částečný reset
      filter[filterBy + '_gte'] = undefined
      filter[filterBy + '_lte'] = undefined

      let dataPickerTimes = this.state.dataPickerTimes
      dataPickerTimes['currentTimeFrom' + currentTimeShort] = moment('2017-01-01')
      dataPickerTimes['currentTimeTo' + currentTimeShort] = moment()
    }

    this.loadDebounced()
    this.forceUpdate()
  }

  render() {
    let contracts = this.state.contracts
    let filter = this.state.filter

    return (
      <div className="row">
        <div className="col-xs-12">
          <div className="box">
            <div className="box-header">
              <h3 className="box-title">Contracts</h3>
              <div className="box-tools">
                <form onSubmit={this.handleSubmit}>
                  <label>
                    <input
                      name="q"
                      type="text"
                      className="form-control input-sm"
                      placeholder="Search contracts"
                      value={filter.q ? filter.q : ""}
                      onChange={this.handleFilter}
                    />
                  </label>
                </form>
              </div>
              <div className="box-body">
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label>Select filter by</label>
                      <div className="input-group">
                        <div className="input-group-addon">
                          <i className="fa fa-filter"></i>
                        </div>
                        <div className="section">
                          <Select
                            name="filter-select"
                            onChange={e => this.handleStatusFilter(e)}
                            options={[
                              { value: 'NEW', label: 'Contracts with status NEW' },
                              { value: 'IN_PROGRESS', label: 'Contracts with status IN_PROGRESS' },
                              { value: 'DONE', label: 'Contracts with status DONE' },
                              { value: 'CANCELED', label: 'Contracts with status CANCELED' },
                            ]}
                            value={this.state.filter.status}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="form-group">
                      <label>Select date range - Data created</label>
                      <div className="form-inline">
                        <div className="form-group">
                          <div className="input-group">
                            <div className="input-group-addon">
                              <i className="fa fa-calendar"></i>
                            </div>
                            <div className="input-group-addon">
                              <DatePicker
                                selected={this.state.dataPickerTimes.currentTimeFromDC}
                                selectsStart
                                startDate={this.state.dataPickerTimes.currentTimeFromDC}
                                endDate={this.state.dataPickerTimes.currentTimeToDC}
                                onChange={e => this.handleDateFilterFrom(e, "dateCreated", "currentTimeFromDC")}
                                placeholderText="Select start date"
                              />
                            </div>
                            <div className="input-group-addon">
                              <DatePicker
                                selected={this.state.dataPickerTimes.currentTimeToDC}
                                selectsEnd
                                startDate={this.state.dataPickerTimes.currentTimeFromDC}
                                endDate={this.state.dataPickerTimes.currentTimeToDC}
                                onChange={e => this.handleDateFilterTo(e, "dateCreated", "currentTimeToDC")}
                                placeholderText="Select end date"
                              />
                            </div>
                            <div className="btn-group">
                              <button type="button" className="btn btn-danger" value="dateCreated" onClick={e => this.handleDateFilterReset("dateCreated", "DC")}>Reset this filter</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="form-group">
                      <label>Select date range - Data edited</label>
                      <div className="form-inline">
                        <div className="form-group">
                          <div className="input-group">
                            <div className="input-group-addon">
                              <i className="fa fa-calendar"></i>
                            </div>
                            <div className="input-group-addon">
                              <DatePicker
                                selected={this.state.dataPickerTimes.currentTimeFromDE}
                                selectsStart
                                startDate={this.state.dataPickerTimes.currentTimeFromDE}
                                endDate={this.state.dataPickerTimes.currentTimeToDE}
                                onChange={e => this.handleDateFilterFrom(e, "dateEdited", "currentTimeFromDE")}
                                placeholderText="Select start date"
                              />
                            </div>
                            <div className="input-group-addon">
                              <DatePicker
                                selected={this.state.dataPickerTimes.currentTimeToDE}
                                selectsEnd
                                startDate={this.state.dataPickerTimes.currentTimeFromDE}
                                endDate={this.state.dataPickerTimes.currentTimeToDE}
                                onChange={e => this.handleDateFilterTo(e, "dateEdited", "currentTimeToDE")}
                                placeholderText="Select end date"
                              />
                            </div>
                            <div className="btn-group">
                              <button type="button" className="btn btn-danger" value="dateEdited" onClick={e => this.handleDateFilterReset("dateEdited", "DE")}>Reset this filter</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="form-group">
                      <label>Select date range - Deadline</label>
                      <div className="form-inline">
                        <div className="form-group">
                          <div className="input-group">
                            <div className="input-group-addon">
                              <i className="fa fa-calendar"></i>
                            </div>
                            <div className="input-group-addon">
                              <DatePicker
                                selected={this.state.dataPickerTimes.currentTimeFromDL}
                                selectsStart
                                startDate={this.state.dataPickerTimes.currentTimeFromDL}
                                endDate={this.state.dataPickerTimes.currentTimeToDL}
                                onChange={e => this.handleDateFilterFrom(e, "deadline", "currentTimeFromDL")}
                                placeholderText="Select start date"
                              />
                            </div>
                            <div className="input-group-addon">
                              <DatePicker
                                selected={this.state.dataPickerTimes.currentTimeToDL}
                                selectsEnd
                                startDate={this.state.dataPickerTimes.currentTimeFromDL}
                                endDate={this.state.dataPickerTimes.currentTimeToDL}
                                onChange={e => this.handleDateFilterTo(e, "deadline", "currentTimeToDL")}
                                placeholderText="Select end date"
                              />
                            </div>
                            <div className="btn-group">
                              <button type="button" className="btn btn-danger" value="deadline" onClick={e => this.handleDateFilterReset("deadline", "DL")}>Reset this filter</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="box-body">
              <div>
                <div className="row">
                  <div className="col-sm-12">
                    <table
                      className="table table-bordered table-striped"
                      role="grid"
                    >
                      <thead>
                        <tr>
                          <th className="col-sm-2">Title</th>
                          <th>Description</th>
                          <th className="col-sm-1">Price</th>
                          <th className="col-sm-2">Customer</th>
                          <th className="col-sm-1">Status</th>
                          <th className="col-sm-1">Created</th>
                          <th className="col-sm-1">Edited</th>
                          <th className="col-sm-1">Deadline</th>
                        </tr>
                      </thead>
                      <tbody>
                        {contracts.map(s => (
                          <tr key={s.id}>
                            <td>
                              <Link
                                to={ROUTES.getUrl(ROUTES.CONTRACT_DETAIL, {
                                  id: s.id
                                })}
                              >
                                {s.name}
                              </Link>
                            </td>
                            <td>{("" + s.description).slice(0, 60)}</td>
                            <td>{price(s.price)}</td>
                            <td>
                              <Link
                                to={ROUTES.getUrl(ROUTES.CUSTOMER_DETAIL, {
                                  id: s.customer.id
                                })}
                              >
                                {s.customer.name}
                              </Link>
                            </td>
                            <td>{s.status}</td>
                            <td>{dateToLocal(s.dateCreated)}</td>
                            <td>
                              {s.dateEdited ? dateToLocal(s.dateEdited) : ""}
                            </td>
                            <td>{s.deadline ? dateToLocal(s.deadline) : ""}</td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-5">
                    <Link to={ROUTES.CONTRACT_NEW} className="btn btn-default">
                      Create new
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div >
    )
  }
}

export default ContractsListing
