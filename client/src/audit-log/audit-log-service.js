import http from '../http'

export default {
  query({entity, operation}) {
    return http.get('/audit/query', {params: {entity, operation}});
  },
}
