import './jquery'
import './tether'

import 'admin-lte'
import 'admin-lte/dist/js/adminlte.min'
import 'bootstrap'
import 'bootstrap/dist/js/bootstrap.min'
import 'admin-lte/plugins/iCheck/icheck.min'